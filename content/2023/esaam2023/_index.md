---
title: "Eclipse SAAM on Cloud-to-Edge Continuum 2023"
headline: "eSAAM 2023"
subtitle: "3rd Eclipse Security, AI, Architecture and Modelling Conference on Cloud to Edge Continuum"
tagline: "October 17, 2023 | Ludwigsburg, Germany"
date: 2023-10-17T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
show_featured_footer: false
hide_call_for_action: true
header_wrapper_class: "header-esaam2023-event"
hide_breadcrumb: true
container: "container-fluid esaam-2023-event"
summary: "eSAAM 2023 will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, specifically  focusing on Security and Privacy, Artificial Intelligence and Machine Learning, Systems and Software Architecture, Modelling and related challenges. This event is co-located with EclipseCon 2023"
layout: single
#main_menu: saam
keywords: ["eclipse", "UOM", "UPM", "conference", "research", "SAAM", "Cloud", "Edge", "IoT", "Cloud-Edge-Continuum", "Edge-Cloud-Continuum", "Cloud Computing", "Security", "AI", "Architecture", "Modelling", "Modeling"]
links: [ [href: cfp.pdf, text: "Call for Papers"], [href: "#dates", text: "Important Dates"], [href: "https://easychair.org/conferences/?conf=esaam2023", text: "Submit your Paper"]]
---

<!-- Introduction -->
{{< grid/section-container id="organizing-committee" class="featured-section-row featured-section-row-light-bg text-center" isMarkdown="false">}}

<h1>Mark your calendars:</h1>
<h2>3rd Eclipse SAAM on Cloud to Edge Continuum 2023 on October 17, 2023!</h2> 
<p align="center">
The conference will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of Cloud-to-Edge continuum, specifically focusing on Security and Privacy, Artificial Intelligence and Machine Learning, Systems and Software Architecture, Modelling and related challenges. <br/> <br/>

The event is co-located with</p>
<a href="https://eclipsecon.org"><img src="images/EclipseCon-Logo-2023-Black.png" width="400"/></a>

<p/><p align="center">
This will be a great opportunity to meet our dynamic <strong>open source community</strong>.<br/><br/>

The <strong>call for papers</strong> is open. You can download it <a href="cfp.pdf">here</a> now.<br/>

All submissions must be in PDF -format and must be submitted online to the <a href="https://easychair.org/conferences/?conf=esaam2023">EasyChair portal</a>.</p>
<p>
<a href="https://eclipsecon.org"><img src="images/ACM_ICPS_v.2B.png" width="300"/></a>
</p>

{{</ grid/section-container >}}

<!-- Topics -->
{{< grid/section-container id="topics" class="featured-section-row featured-section-row-highligthed-bg text-center">}}

<h2>Technical topics of interest in Cloud Computing</h2>

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Security and Privacy for the Cloud to Edge Continuum](images/security-black.png)](topics/index.html#security-and-privacy-for-the-cloud-to-edge-continuum)
### Security and Privacy
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Artificial Intelligence for the Cloud to Edge Continuum](images/ai-black.png)](topics/index.html#artificial-intelligence-for-the-cloud-to-edge-continuum)
### Artificial Intelligence
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Architectures for the Cloud to Edge Continuum](images/icon-architecture.png)](topics/index.html#architectures-for-the-cloud-to-edge-continuum)
### Architecture
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Modelling for the Cloud to Edge Continuum](images/modeling-black.png)](topics/index.html#modelling-for-the-cloud-to-edge-continuum)
### Models and Services
{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}


[//]: # (Dates)
{{< grid/section-container id="dates" class="featured-section-row featured-section-row-light-bg text-center">}}

<h2>Important Dates</h2>

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
![Paper submission deadline](images/date-07-09.png)
### New Paper Submission Deadline
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
![Acceptance Notification](images/date-07-31.png)
### Acceptance Notification
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
![Camera-Ready Paper Submission](images/date-09-08.png)
### Camera-Ready Paper Submission
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
![Conference Dates](images/date-10-17.png)
## Conference Date
{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}


<!-- Proceedings -->
<!--
{{< grid/section-container id="proceedings" class="featured-section-row featured-section-row-lighter-bg text-center">}}
	<h2>Proceedings</h2>
	<h4>The papers will be peer reviewed. Selected papers will be published under the <a href="https://creativecommons.org/licenses/by/4.0/">CC BY 4 .0</a> license on <a href="http://ceur-ws.org">the CEUR portal</a>.</h4>
    <hr/>
    <h3>Indexed by:</h3>
{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
  [![Scopus](images/scopus_logo.png)](https://www.scopus.com/)
{{</ grid/div >}}
 
{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
  [![Google Scholar](images/GScholar_logo.png)](https://scholar.google.com/)
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
  [![dblp](images/dblp_logo.png)](https://dblp.org)
{{</ grid/div >}}
 
{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
  [![Semantic Scholar](images/semantic_scholar_logo.png)](https://www.semanticscholar.org)
{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}
-->

[//]: # (Agenda)
<!--
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
  {{< events/agenda event="saam-cloud" year="2022" >}}
{{</ grid/section-container >}}
-->
[//]: # (TCP)


<!-- PROGRAM COMMITTEE --> 
{{< grid/section-container id="program-committee" class="featured-section-row featured-section-row-highligthed-bg">}}
<h2>Technical Program Committee</h2>

<p>The Technical Program Committee is an independent panel of expert volunteers and as such will do their best to judge papers objectively and on the principle of a level playing field for all. </p>

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-12 padding-bottom-20" isMarkdown="true">}}
* Apostolos  Ampatzoglou, University of Macedonia
* Vasilios  Andrikopoulos, University of Groningen
* Luca  Anselma, University of Turin
* Nuno Antunes, University of Coimbra
* Alessandra  Bagnato, SOFTEAM
* Rami  Bahsoon, University of Birmingham
* Peter  Bednar, TUKE
* Samira Briongos, NEC Laboratories Europe
* Benoit Combemale, University of Rennes
* Davide  Conzon , LINKS Foundation
* Gil  Gonçalves, FEUP
{{</ grid/div >}}

{{< grid/div class="col-md-12 padding-bottom-20" isMarkdown="true">}}
* Fulya Horozal, ATB
* Teodoro  Montanaro, Università del Salento
* Tero    Päivärinta, Luleå University of Technology
* Fabio Palomba, University of Salerno
* Panagiotis  Papadimitriou, University of Macedonia
* Ella Peltonen, University of Oulu
* Eliseu  Pereira, FEUP
* Sebastian Scholze, ATB
* Miltiadis Siavvas, CERTH
* Dimitris Syrivelis, NVIDIA, Israel
* Dimitrios Tsoukalas, CERTH
{{</ grid/div >}}
{{</ grid/div >}}

{{</ grid/section-container >}}


[//]: # (Speakers)
<!--
{{< grid/section-container id="keynote" class="featured-section-row text-center featured-section-row-dark-bg eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="saam-cloud" year="2022" title="Keynotes Speakers" source="keynote" imageRoot="/2022/saam-cloud/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}
-->

[//]: # (Organizing Committee)
{{< grid/section-container id="organizing-committee" class="featured-section-row featured-section-row-light-bg text-center" isMarkdown="true">}}

## Organizing Committee 

eSAAM 2023 conference is co-organized by

**the University of Macedonia, Universidad Politecnica de Madrid, and Eclipse Foundation.**

### Conference Co-chairs
**Alexandros Chatzigeorgiou**, University of Macedonia\
**David Jimenez**, Universidad Politecnica de Madrid\
**Philippe Krief**, Eclipse Foundation

### Program Committee Chair
**Alexandros Chatzigeorgiou**, University of Macedonia\
**David Jimenez**, Universidad Politécnica de Madrid\
**Rosaria Rossini**, Eclipse Foundation

<!--
### Publicity  
Silvia Miles, Eclipse Foundation Europe, Germany
-->

{{</ grid/section-container >}}


<!-- ORGANIZERS -->
{{< grid/section-container id="organizers" class="featured-section-row featured-section-row-highligthed-bg text-center">}}
  {{< events/sponsors event="esaam2023" year="2023" source="coorganizers" title="Co-organizers" useMax="false" displayBecomeSponsor="false">}}
{{</ grid/section-container >}}

<!-- SPONSORS -->
<!--
{{< grid/section-container id="sponsors" class="featured-section-row featured-section-row-light-bg text-center">}}
  {{< events/sponsors event="saam-cloud" year="2022" source="sponsors" title="Sponsors" useMax="false" displayBecomeSponsor="false">}}
{{</ grid/section-container >}}
--> 
[//]: # (Organizing Committee)
{{< grid/section-container id="organizing-committee" class="featured-section-row featured-section-row-light-bg" isMarkdown="true">}}
## Previous conferences
* [eSAAM on Mobility 2021](https://events.eclipse.org/2021/saam-mobility/)
* [eSAM IoT 2020](https://events.eclipse.org/2020/sam-iot/)

{{</ grid/section-container >}}

{{< grid/section-container id="organizing-committee" class="featured-section-row featured-section-row-highligthed-bg" isMarkdown="true">}}
## Images from
* [Header by Roman from Pixabay](https://pixabay.com/illustrations/cloud-computer-circuit-board-cpu-6532831/)
* [Security and Privacy icon by F.Adiima from NounProject](https://thenounproject.com/search/?q=security&i=3349833)
* [Artificial Intelligence icon by priyanka from NounProject](https://thenounproject.com/search/?q=artificial%20intelligence&i=2858867)
* [Architecture icon by SBTS from NounProject](https://thenounproject.com/icon/web-architecture-1711291)
* [Models and Services icon by G.Tachfin from NounProject](https://thenounproject.com/search/?q=modelling&i=2710243)
{{</ grid/section-container >}}


